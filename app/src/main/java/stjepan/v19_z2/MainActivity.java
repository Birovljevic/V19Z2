package stjepan.v19_z2;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.ibUp) ImageView ibUp;
    @BindView(R.id.ibLeft) ImageView ibLeft;
    @BindView(R.id.ibRight) ImageView ibRight;
    @BindView(R.id.ibDown) ImageView ibDown;

    SoundPool mSoundPool;
    boolean mIsLoaded = false;
    private HashMap<Integer, Integer> mSoundMap = new HashMap<>();

    public static final String MSG_KEY = "msg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadImages();
        this.loadSounds();

    }
    private void loadImages() {
        Picasso.with(getApplicationContext()).load(R.drawable.arrow_up).fit().centerCrop().into(ibUp);
        Picasso.with(getApplicationContext()).load(R.drawable.arrow_left).fit().centerCrop().into(ibLeft);
        Picasso.with(getApplicationContext()).load(R.drawable.arrow_right).fit().centerCrop().into(ibRight);
        Picasso.with(getApplicationContext()).load(R.drawable.arrow_down).fit().centerCrop().into(ibDown);
    }
    private void loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = new SoundPool.Builder().setMaxStreams(10).build();
        }else{
            this.mSoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC,0);
        }

        this.mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                Log.d("Test",String.valueOf(sampleId));
                mIsLoaded = true;
            }
        });
        this.mSoundMap.put(R.raw.robot_go, this.mSoundPool.load(this, R.raw.robot_go,1));
        this.mSoundMap.put(R.raw.robot_turn_left, this.mSoundPool.load(this, R.raw.robot_turn_left,1));
        this.mSoundMap.put(R.raw.robot_turn_right,this.mSoundPool.load(this, R.raw.robot_turn_right,1));
        this.mSoundMap.put(R.raw.robot_stop,this.mSoundPool.load(this, R.raw.robot_stop,1));
    }

    @OnClick({R.id.ibUp, R.id.ibLeft, R.id.ibRight, R.id.ibDown})
    public void giveDirections(ImageButton button){
        String direction = "";
        switch (button.getId()){
            case R.id.ibUp: direction = "Go";playSound(R.raw.robot_go); break;
            case R.id.ibLeft: direction = "Left"; playSound(R.raw.robot_turn_left); break;
            case R.id.ibRight: direction = "Right"; playSound(R.raw.robot_turn_right); break;
            case R.id.ibDown: direction = "Stop"; playSound(R.raw.robot_stop); break;
        }
        showNotification(direction);
    }

    private void showNotification(String direction) {
        // Set-up of the required data:
        int notificationID = 1;
        String message = "Direction: " + direction;
        String title = "Directions";
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.app_icon);
        Intent intent = new Intent(this, DisplayActivity.class);
        intent.putExtra(MSG_KEY, "This is a message from the notification.");
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT
        );
        // Creating the notification:
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(bitmap)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLights(Color.RED, 2000, 1000)
                .setVibrate(new long[]{1000,1000,1000,1000,1000})
                .setSound(soundUri);
        Notification notification = builder.build();

        // Actual notification:
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, notification);

        // This activity is no longer needed, so we can shut it down:
        //this.finish(); --- ubija main activity
    }

    private void playSound(int resourceId) {
        int soundID = this.mSoundMap.get(resourceId);
        this.mSoundPool.play(soundID, 1,1,1,0,1F);
    }
}
